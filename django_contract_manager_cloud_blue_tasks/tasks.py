import secrets

from celery import shared_task

from django_contract_manager_cloud_blue.django_contract_manager_cloud_blue.func import *
from django_contract_manager_cloud_blue.django_contract_manager_cloud_blue.models import *
from django_directory_backend_client.django_directory_backend_client.func import (
    directory_backend_get_tenant,
)


@shared_task(name="cloud_blue_detect_customers")
def cloud_blue_detect_customers():
    customers = MdatCustomers.objects.filter(
        enabled=True,
    ).filter(cloudbluecustomermapping__isnull=True)

    for customer in customers:
        CloudBlueCustomerMapping.objects.create(
            customer=customer,
        )

    return


@shared_task(name="cloud_blue_create_customers")
def cloud_blue_create_customers():
    new_customers = CloudBlueCustomerMapping.objects.filter(
        cloud_blue_id__isnull=True,
    ).filter(
        customer__org_tag__isnull=False,
    )

    cb_token = cb_fetch_token()

    for new_customer in new_customers:
        # fetch superadmin from directory
        tenant = directory_backend_get_tenant(
            new_customer.customer.id, new_customer.customer.org_tag
        )
        super_admin_upn = tenant["super_admin_upn"]

        customer_billing_address = (
            new_customer.customer.mdatcustomerbillingaddresses.address
        )

        cb_customer_data = {
            "externalId": str(new_customer.customer.id),
            "name": new_customer.customer.name,
            "language": "de",
            "address": {
                "streetAddress": "{} {}".format(
                    customer_billing_address.street.title,
                    customer_billing_address.house_nr,
                ),
                "postalCode": customer_billing_address.street.city.zip_code,
                "city": customer_billing_address.street.city.title,
                "state": customer_billing_address.street.city.title,
                "countryCode": customer_billing_address.street.city.country.iso,
            },
            "contactPersons": [
                {
                    "type": "admin",
                    "firstName": "Super",
                    "lastName": "Admin",
                    "email": super_admin_upn,
                    "phoneNumber": "49##219285490#",
                    "login": super_admin_upn,
                    "password": secrets.token_urlsafe(32),
                }
            ],
        }

        new_cb_customer = cb_customers_create(cb_token, cb_customer_data)

        try:
            new_customer.cloud_blue_id = new_cb_customer["id"]
        except KeyError:
            # workaround: cloud blue sometimes fails during creation, try customer fetch instead
            cb_cust_list = cb_customers_list(cb_token)

            for cb_customer in cb_cust_list:
                if cb_customer["externalId"] == str(new_customer.customer.id):
                    new_customer.cloud_blue_id = cb_customer["id"]

        new_customer.save()

    return


@shared_task(name="cloud_blue_sync_products")
def cloud_blue_sync_products():
    cb_token = cb_fetch_token()

    cb_products = cb_products_list(cb_token)

    for cb_product in cb_products:
        try:
            cb_item = CloudBlueProducts.objects.get(
                cb_item_code=cb_product["mpn"],
            )
        except CloudBlueProducts.DoesNotExist:
            cb_item = CloudBlueProducts(
                cb_item_code=cb_product["mpn"],
            )

        cb_item.cb_item_name = cb_product["name"]

        cb_item.save()

        cb_item.refresh_from_db()

        # check for MDAT item
        new_external_id = "MP-C" + str(1000000 + cb_item.id)

        if cb_item.item is not None:
            mdat_item = MdatItems.objects.using("mdat").get(id=cb_item.item_id)
        else:
            try:
                mdat_item = MdatItems.objects.using("mdat").get(
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                    external_id=new_external_id,
                )
            except MdatItems.DoesNotExist:
                mdat_item = MdatItems(
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                    external_id=new_external_id,
                    linked_to_id=1,
                )

        mdat_item.name = cb_item.cb_item_name
        mdat_item.manufacturer_sku = cb_item.cb_item_code
        mdat_item.itscope_skip = True

        mdat_item.save(using="mdat")

        if "prices" in cb_product:
            for price in cb_product["prices"]:
                if price["type"] == "recurring":
                    if price["currency"] == "EUR":
                        new_price = Decimal(price["amount"])

                        if mdat_item.price != new_price:
                            try:
                                mdat_price = MdatItemPrices.objects.using("mdat").get(
                                    item_id=mdat_item.id,
                                    valid_from=datetime.today(),
                                )
                            except MdatItemPrices.DoesNotExist:
                                mdat_price = MdatItemPrices(
                                    item_id=mdat_item.id,
                                    valid_from=datetime.today(),
                                )

                            mdat_price.price = new_price

                            mdat_price.save(using="mdat")

                    else:
                        print("WARNING, NOT EUR")

        if mdat_item.linked_to_id != mdat_item.id:
            mdat_item.linked_to_id = mdat_item.id
            mdat_item.save(using="mdat")

        mdat_item.refresh_from_db()

        mdat_item.save(using="default")

        cb_item.item = mdat_item

        cb_item.save()

    return
