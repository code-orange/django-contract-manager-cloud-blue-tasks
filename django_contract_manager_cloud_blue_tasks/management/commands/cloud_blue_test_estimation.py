from pprint import pprint

from django.core.management.base import BaseCommand

from django_contract_manager_cloud_blue.django_contract_manager_cloud_blue.func import (
    cb_fetch_token,
    cb_orders_estimate,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        # fetch cloud token
        cb_token = cb_fetch_token()

        request_data = {
            "customerId": "1234567",
            "products": (
                {
                    "mpn": "CFQ7TTC0LGZT:0001",
                    "quantity": 1,
                    "billingPeriod": {"type": "year", "duration": 1},
                    "subscriptionPeriod": {"type": "year", "duration": 1},
                    "parameters": (
                        {
                            "name": "mpnid",
                            "value": "3874691",
                        },
                        {
                            "name": "microsoft_domain",
                            "value": "example.onmicrosoft.com",
                        },
                    ),
                },
            ),
            "type": "sales",
        }

        # test estimation
        estimation = cb_orders_estimate(cb_token, request_data)

        pprint(estimation)
