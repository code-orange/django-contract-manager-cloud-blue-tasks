from django.core.management.base import BaseCommand

from django_contract_manager_cloud_blue_tasks.django_contract_manager_cloud_blue_tasks.tasks import (
    cloud_blue_create_customers,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        cloud_blue_create_customers()
