from django.core.management.base import BaseCommand
from openpyxl import load_workbook
from django.conf import settings

from django_contract_manager_cloud_blue.django_contract_manager_cloud_blue.func import (
    cb_fetch_token,
    cb_orders_create,
)
from django_contract_manager_cloud_blue.django_contract_manager_cloud_blue.models import (
    CloudBlueCustomerMapping,
)
from django_contract_manager_microsoft.django_contract_manager_microsoft.func import (
    get_tenant_id_by_domain,
)
from django_contract_manager_models.django_contract_manager_models.models import (
    ContractCustSettings,
)
from django_mdat_customer.django_mdat_customer.models import MdatCustomers


class Command(BaseCommand):
    def handle(self, *args, **options):
        cb_token = cb_fetch_token()

        wb = load_workbook(filename="adn_subscriptions.xlsx")

        subscriptions = wb["Subscriptions"]

        for row in subscriptions.rows:
            if row[0].row == 1:
                continue

            sap_customer_nr = str(row[15].value)
            mpn = str(row[7].value).lower()
            quantity = int(float(row[9].value))

            print(
                "Purchasing " + str(quantity) + "x " + mpn + " for " + sap_customer_nr
            )

            # Process
            customer = MdatCustomers.objects.get(
                external_id=sap_customer_nr,
                created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
            )

            contract_tenant = ContractCustSettings.objects.get(customer=customer)

            ms_tenant_domain = contract_tenant.ms_tenant_domain
            ms_tenant_short = ms_tenant_domain.split(".")[0]
            ms_tenant_id = get_tenant_id_by_domain(contract_tenant.ms_tenant_domain)

            cloud_blue_customer = CloudBlueCustomerMapping.objects.get(
                customer=customer
            )

            new_order_data = {
                "customerId": str(cloud_blue_customer.cloud_blue_id),
                "products": (
                    {
                        "mpn": mpn,
                        "quantity": quantity,
                        "parameters": (
                            {
                                "name": "mpnid",
                                "value": "3874691",
                            },
                            {
                                "name": "domain",
                                "value": ms_tenant_short,
                            },
                            {
                                "name": "msAccountId",
                                "value": ms_tenant_id,
                            },
                        ),
                    },
                ),
                "type": "sales",
            }

            new_order = cb_orders_create(cb_token, new_order_data)
