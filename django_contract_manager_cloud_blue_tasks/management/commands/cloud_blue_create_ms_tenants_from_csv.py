import csv
from pprint import pprint

from django.core.management.base import BaseCommand
from django.conf import settings

from django_contract_manager_cloud_blue.django_contract_manager_cloud_blue.func import (
    cb_fetch_token,
    cb_orders_create,
)
from django_contract_manager_cloud_blue.django_contract_manager_cloud_blue.models import (
    CloudBlueCustomerMapping,
)
from django_mdat_customer.django_mdat_customer.models import MdatCustomers


class Command(BaseCommand):
    def handle(self, *args, **options):
        mpn = "6448ae60-0ea4-4164-8ea9-c2922c757f3d"
        quantity = 25

        # read requested customers
        csv_file = open("new_ms_tenants.csv")

        csv_reader = csv.reader(csv_file, delimiter=";")

        # skip header
        next(csv_reader)

        # fetch cloud token
        cb_token = cb_fetch_token()

        for row in csv_reader:
            sap_customer_nr = str(row[0])
            domain = str(row[1])

            print(
                "Purchasing " + str(quantity) + "x " + mpn + " for " + sap_customer_nr
            )

            # Process
            customer = MdatCustomers.objects.get(
                external_id=sap_customer_nr,
                created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
            )

            cloud_blue_customer = CloudBlueCustomerMapping.objects.get(
                customer=customer
            )

            new_order_data = {
                "customerId": str(cloud_blue_customer.cloud_blue_id),
                "products": (
                    {
                        "mpn": mpn,
                        "quantity": quantity,
                        "parameters": (
                            {
                                "name": "mpnid",
                                "value": "3874691",
                            },
                            {
                                "name": "domain",
                                "value": domain,
                            },
                        ),
                    },
                ),
                "type": "sales",
            }

            pprint(new_order_data)

            new_order = cb_orders_create(cb_token, new_order_data)

            pprint(new_order)
